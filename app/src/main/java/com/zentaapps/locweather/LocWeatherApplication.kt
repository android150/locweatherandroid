package com.zentaapps.locweather

import android.app.Application
import com.zentaapps.locweather.di.generalModules
import com.zentaapps.locweather.di.viewModelModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class LocWeatherApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@LocWeatherApplication)
            modules(generalModules, viewModelModules)
        }
    }
}