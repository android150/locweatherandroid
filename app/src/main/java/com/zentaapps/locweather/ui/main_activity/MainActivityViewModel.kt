package com.zentaapps.locweather.ui.main_activity

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.zentaapps.locweather.error_handling.Status
import com.zentaapps.locweather.models.ResponseWeather
import com.zentaapps.locweather.network.ApiRepository
import com.zentaapps.locweather.util.WeatherDateUtil
import kotlinx.coroutines.launch


class MainActivityViewModel(private val apiRepository: ApiRepository) : ViewModel() {
    private val tag = "MainActivityViewModel"

    private var _isCameraMoving = MutableLiveData<Boolean>()
    val isCameraMoving: LiveData<Boolean>
        get() = _isCameraMoving


    private var _hasConnection = MutableLiveData(true)
    val hasConnection: LiveData<Boolean>
        get() = _hasConnection

    var mainTemp = MutableLiveData("N/A")
    var mainIcon = MutableLiveData<Int>()

    private var requestCount = 0

    private var _weather = MutableLiveData<ResponseWeather>()
    val weather: LiveData<ResponseWeather>
        get() = _weather


    fun cameraIsMoving(isMoving: Boolean) {
        _isCameraMoving.value = isMoving
    }


    fun getWeather(latLng: LatLng) {
        Log.i(tag, "weather: Calling")
        viewModelScope.launch {
            val weather = apiRepository.getWeather(latLng)
            if (weather.status == Status.OK) {
                _weather.postValue(weather.data)
                Log.i(tag, "weather: ${WeatherDateUtil.getDay(weather.data?.daily?.get(0)?.date)}")
                requestCount = 0
                return@launch
            }
            Log.e(tag, "weather: Failed. reason: ${weather.message}")
            if (++requestCount < 10) {
                getWeather(latLng)
            } else {
                _hasConnection.value = false
            }
        }
    }

}