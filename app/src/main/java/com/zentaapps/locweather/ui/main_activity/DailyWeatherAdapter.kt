package com.zentaapps.locweather.ui.main_activity

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.zentaapps.locweather.R
import com.zentaapps.locweather.databinding.ItemWeatherBinding
import com.zentaapps.locweather.models.DailyWeather
import com.zentaapps.locweather.util.WeatherDateUtil
import com.zentaapps.locweather.util.WeatherIcons

class DailyWeatherAdapter(private val context: Context) :
    ListAdapter<DailyWeather, DailyWeatherAdapter.DailyWeatherViewHolder>(
        DailyWeatherDiffCallback()
    ) {


    class DailyWeatherViewHolder(val b: ItemWeatherBinding) : RecyclerView.ViewHolder(b.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyWeatherViewHolder {
        val inflater = LayoutInflater.from(context)
        val b = ItemWeatherBinding.inflate(inflater, parent, false)
        return DailyWeatherViewHolder(b)
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: DailyWeatherViewHolder, position: Int) {
        val entry = currentList[position]
        holder.b.tvTitle.text = WeatherDateUtil.getDay(entry.date)
        holder.b.imgIcon.setImageResource(
            WeatherIcons().getLocalIcon(
                entry.body?.get(0)?.iconName ?: ""
            )
        )
        Log.i("TAG", "onBindViewHolder: ${holder.b.layoutRoot.width}x${holder.b.layoutRoot.height}")
        holder.b.tvTemp.text =
            context.getString(R.string.temp_degree_sample, entry.temp.day?.toInt())
        holder.b.tvWeatherInfo.text = entry.body?.get(0)?.status ?: "N/A"

    }
}

class DailyWeatherDiffCallback : DiffUtil.ItemCallback<DailyWeather>() {
    override fun areItemsTheSame(oldItem: DailyWeather, newItem: DailyWeather): Boolean {
        return oldItem.date == newItem.date
    }

    override fun areContentsTheSame(oldItem: DailyWeather, newItem: DailyWeather): Boolean {
        return oldItem == newItem
    }

}