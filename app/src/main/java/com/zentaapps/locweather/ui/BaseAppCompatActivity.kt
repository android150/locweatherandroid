package com.zentaapps.locweather.ui

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.zentaapps.locweather.R

abstract class BaseAppCompatActivity<B : ViewDataBinding, VM : ViewModel>(@LayoutRes private val layoutResource: Int) :
    AppCompatActivity() {
    lateinit var b: B
    abstract val viewModel: VM
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        b = DataBindingUtil.inflate(
            layoutInflater,
            layoutResource,
            findViewById(R.id.root),
            false
        )
        b.lifecycleOwner = this
        setContentView(b.root)
        setBindingViewModel()
        init()
    }

    abstract fun setBindingViewModel()

    abstract fun init()
}