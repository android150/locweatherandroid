package com.zentaapps.locweather.ui.main_activity

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.core.app.ActivityCompat
import com.ethanhua.skeleton.Skeleton
import com.ethanhua.skeleton.ViewSkeletonScreen
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.zentaapps.locweather.R
import com.zentaapps.locweather.databinding.ActivityMainBinding
import com.zentaapps.locweather.ui.BaseAppCompatActivity
import com.zentaapps.locweather.util.WeatherIcons
import org.koin.android.ext.android.inject


class MainActivity :
    BaseAppCompatActivity<ActivityMainBinding, MainActivityViewModel>(R.layout.activity_main),
    OnMapReadyCallback, LocationListener {
    private val requestLocation = 99
    override val viewModel: MainActivityViewModel by inject()
    private var todaySkeleton: ViewSkeletonScreen? = null
    private lateinit var map: GoogleMap
    private var initState = true
    override fun setBindingViewModel() {
        b.viewModel = viewModel
    }

    override fun init() {
        val fragMap = supportFragmentManager.findFragmentById(R.id.frag_map) as SupportMapFragment
        fragMap.getMapAsync(this)

        //Observe when data comes
        viewModel.weather.observe(this) {
            viewModel.mainIcon.value =
                WeatherIcons().getLocalIcon(it.current?.body?.get(0)?.iconName!!)
            viewModel.mainTemp.value =
                getString(R.string.temp_degree_sample, it.current.temp?.toInt())
            val adapter = DailyWeatherAdapter(this)
            val list = it.daily as ArrayList
            list.removeAt(0)
            adapter.submitList(list)
            b.rvDaily.invalidate()
            b.rvDaily.adapter = adapter
            showSkeleton(false)
        }


        //observe when no internet detected
        viewModel.hasConnection.observe(this) {
            if (!it) {
                AlertDialog.Builder(this)
                    .setTitle(R.string.no_connection_title)
                    .setMessage(R.string.no_connection_message)
                    .setPositiveButton(
                        R.string.ok
                    ) { dialog, _ -> dialog?.dismiss() }
                    .setOnDismissListener { finish() }
                    .show()
            }
        }

        //Update now icon on every data update
        viewModel.mainIcon.observe(this) {
            b.imgToday.setImageResource(it)
        }
    }

    //Track camera movement and order for animations and update date
    private fun watchCameraMovement(map: GoogleMap) {
        viewModel.isCameraMoving.observe(this) {
            if (it) {
                animateViews(false)
            } else {
                animateViews(true)
                showSkeleton(true)
                val latLng = map.cameraPosition.target
                viewModel.getWeather(latLng)
            }
        }
    }

    //Shows or hides the skeleton mask
    private fun showSkeleton(show: Boolean) {
        if (show) {
            Skeleton.bind(b.rvDaily)
                .adapter(b.rvDaily.adapter)
                .load(R.layout.item_weather_mask)
                .angle(0)
                .count(7)
                .show()
            if (todaySkeleton == null) {
                todaySkeleton = Skeleton.bind(b.layoutTodayContent)
                    .load(R.layout.weather_today_mask)
                    .angle(0)
                    .show()
            } else {
                todaySkeleton!!.show()
            }
        } else {
            todaySkeleton?.apply {
                hide()
            }
        }
    }

    //Manage animation of views
    private fun animateViews(isShow: Boolean) {
        val duration = 250L
        val verticalOffset = 0F
        val horizontalOffset = 50F
        if (isShow) {//out
            b.layoutToday.animate()
                .translationX(0F)
                .setDuration(duration)
                .start()
            b.rvDaily.animate()
                .translationY(0F)
                .setDuration(duration)
                .start()
        } else {//in
            b.layoutToday.animate()
                .translationX(-b.layoutToday.width - horizontalOffset)
                .setDuration(duration)
                .start()
            b.rvDaily.animate()
                .translationY(b.rvDaily.height + verticalOffset)
                .setDuration(duration)
                .start()
        }
    }

    //Runs when map ready
    override fun onMapReady(googleMap: GoogleMap) {
        initMap(googleMap)
    }

    //First init for the map
    private fun initMap(googleMap: GoogleMap) {
        map = googleMap
        watchCameraMovement(googleMap)
        setMapListeners(googleMap)
        enableMyLocation(googleMap)



        getLocation()
    }

    //Configure map listeners
    private fun setMapListeners(googleMap: GoogleMap) {
        googleMap.setOnCameraMoveStartedListener {
            viewModel.cameraIsMoving(true)
        }
        googleMap.setOnCameraIdleListener {
            viewModel.cameraIsMoving(false)
        }
    }

    //Request for location permission for map
    private fun enableMyLocation(googleMap: GoogleMap) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            googleMap.isMyLocationEnabled = true
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestLocation
            )
        }
    }

    //Moves to requested latlng
    private fun moveToLocation(location: LatLng) {
        val cameraPosition = CameraPosition.Builder()
            .target(location)
            .build()
        val cu = CameraUpdateFactory.newCameraPosition(cameraPosition)
        map.animateCamera(cu)
    }


    //get user location
    private fun getLocation() {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)
        }
    }

    // Listen to user's location changes
    override fun onLocationChanged(location: Location) {
        if (initState) {
            initState = false
            moveToLocation(LatLng(location.latitude, location.longitude))
        }
    }

}