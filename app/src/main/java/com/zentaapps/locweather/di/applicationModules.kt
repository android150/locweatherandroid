package com.zentaapps.locweather.di

import com.zentaapps.locweather.network.provideApiRepo
import com.zentaapps.locweather.network.provideApiService
import com.zentaapps.locweather.network.provideResponseHandler
import com.zentaapps.locweather.ui.main_activity.MainActivityViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val generalModules = module {
    single { provideApiRepo(get(), get()) }
    single { provideApiService() }
    single { provideResponseHandler() }
}
val viewModelModules = module {
    viewModel { MainActivityViewModel(get()) }
}