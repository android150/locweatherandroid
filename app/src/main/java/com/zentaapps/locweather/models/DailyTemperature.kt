package com.zentaapps.locweather.models

import com.google.gson.annotations.SerializedName

data class DailyTemperature(
    @SerializedName(value = "day")
    val day: Float?,
    @SerializedName(value = "night")
    val night: Float?,
    @SerializedName(value = "min")
    val minimum: Float?,
    @SerializedName(value = "max")
    val maximum: Float?,
    @SerializedName(value = "morn")
    val morning: Float?,
    @SerializedName(value = "eve")
    val evening: Float?,
)
