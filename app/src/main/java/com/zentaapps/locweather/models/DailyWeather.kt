package com.zentaapps.locweather.models

import com.google.gson.annotations.SerializedName

data class DailyWeather(
    @SerializedName("dt")
    val date: Long?,
    @SerializedName(value = "temp")
    val temp: DailyTemperature,
    @SerializedName(value = "weather")
    val body: List<WeatherInfo>?

)
