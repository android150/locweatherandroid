package com.zentaapps.locweather.models

import com.google.gson.annotations.SerializedName

data class ResponseWeather(
    @SerializedName(value = "current")
    val current: CurrentWeather?,
    @SerializedName(value = "daily")
    val daily: List<DailyWeather>?

)
