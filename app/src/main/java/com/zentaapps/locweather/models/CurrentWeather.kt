package com.zentaapps.locweather.models

import com.google.gson.annotations.SerializedName

data class CurrentWeather(
    @SerializedName(value = "temp")
    val temp: Float?,
    @SerializedName(value = "weather")
    val body: List<WeatherInfo>?
)