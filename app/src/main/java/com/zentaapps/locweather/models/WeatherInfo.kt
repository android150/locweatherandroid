package com.zentaapps.locweather.models

import com.google.gson.annotations.SerializedName

data class WeatherInfo(
    @SerializedName(value = "id")
    val id: Int?,
    @SerializedName(value = "main")
    val status: String?,
    @SerializedName(value = "description")
    val description: String?,
    @SerializedName(value = "icon")
    val iconName: String?
)