package com.zentaapps.locweather.error_handling

import java.lang.Exception

class ResponseHandler {
    fun <T> handleSuccess(data: T): Resource<T> {
        return Resource.success(data)
    }

    fun <T> handleError(e: Exception): Resource<T> {
        return Resource.error(null, e.message)
    }
}