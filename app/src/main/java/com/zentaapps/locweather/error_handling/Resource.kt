package com.zentaapps.locweather.error_handling

data class Resource<out T>(
    val status: Status,
    val data: T?,
    val message: String?
) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.OK, data, "ok")
        }

        fun <T> error(data: T?, message: String?): Resource<T> {
            return Resource(Status.FAILED, data, message)
        }
    }
}