package com.zentaapps.locweather.error_handling

enum class Status {
    OK, FAILED
}