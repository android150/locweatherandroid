package com.zentaapps.locweather.network

import com.google.android.gms.maps.model.LatLng
import com.zentaapps.locweather.error_handling.Resource
import com.zentaapps.locweather.error_handling.ResponseHandler
import com.zentaapps.locweather.models.ResponseWeather
import java.lang.Exception

class ApiRepository(
    private val apiService: IOpenWeather,
    private val responseHandler: ResponseHandler
) {
    suspend fun getWeather(latLng: LatLng): Resource<ResponseWeather> {
        return try {
            val response = apiService.getWeather(
                latLng.latitude,
                latLng.longitude
            )
            responseHandler.handleSuccess(response)
        } catch (e: Exception) {
            responseHandler.handleError(e)
        }

    }
}