package com.zentaapps.locweather.network

import com.zentaapps.locweather.error_handling.ResponseHandler

fun provideApiService(): IOpenWeather {
    return ApiService.getClient()
}

fun provideResponseHandler(): ResponseHandler {
    return ResponseHandler()
}

fun provideApiRepo(apiService: IOpenWeather, responseHandler: ResponseHandler): ApiRepository {
    return ApiRepository(apiService, responseHandler)
}