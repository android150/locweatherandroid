package com.zentaapps.locweather.network

import com.zentaapps.locweather.util.OPEN_WEATHER_BASE_API_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiService {
    inline fun <reified T> getClient(): T {
        return Retrofit.Builder()
            .baseUrl(OPEN_WEATHER_BASE_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(T::class.java)
    }
}