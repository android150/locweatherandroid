package com.zentaapps.locweather.network


import com.zentaapps.locweather.models.ResponseWeather
import com.zentaapps.locweather.util.OPEN_WEATHER_API_KEY
import retrofit2.http.GET
import retrofit2.http.Query

interface IOpenWeather {
    //https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&exclude={part}&appid={API%20key}
    @GET("onecall?appid=$OPEN_WEATHER_API_KEY&units=metric&exclude=minutely,hourly")//standard,imperial,metric
    suspend fun getWeather(
        @Query("lat") lat: Double,
        @Query("lon") lng: Double
    ): ResponseWeather
}