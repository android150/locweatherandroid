package com.zentaapps.locweather.util

import androidx.annotation.LayoutRes
import com.zentaapps.locweather.R


class WeatherIcons {
    //doc https://openweathermap.org/weather-conditions

    @LayoutRes
    fun getLocalIcon(iconName: String): Int {
        return when (iconName) {
            "01d" -> R.drawable.ic_sunny_day
            "02d" -> R.drawable.ic_cloudy_day
            "03d" -> R.drawable.ic_cloud_white
            "04d" -> R.drawable.ic_cloud_white
            "09d" -> R.drawable.ic_rainy
            "10d" -> R.drawable.ic_rainy_day
            "11d" -> R.drawable.ic_bolt
            "13d" -> R.drawable.ic_snowfall
            "50d" -> R.drawable.ic_fog
            "01n" -> R.drawable.ic_full_moon
            "02n" -> R.drawable.ic_cloudy_night
            "03n" -> R.drawable.ic_cloud_white
            "04n" -> R.drawable.ic_cloud_white
            "09n" -> R.drawable.ic_rainy
            "10n" -> R.drawable.ic_rainy_night
            "11n" -> R.drawable.ic_bolt
            "13n" -> R.drawable.ic_snowfall
            "50n" -> R.drawable.ic_fog
            else -> R.drawable.ic_sunny_day
        }
    }
}