package com.zentaapps.locweather.util


import android.annotation.SuppressLint
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

object WeatherDateUtil {
    @SuppressLint("SimpleDateFormat")
    fun getDay(timestamp: Long?): String {
        return try {
            val date = Date(timestamp!! * 1000)
            SimpleDateFormat("EEEE").format(date)
        } catch (e: Exception) {
            "N/A"
        }
    }
}